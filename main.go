package readbyline

import (
	"bufio"
	"io"
	"os"
	"strings"
)

//ReadLine
func ReadLine(fileName string) []string {
	re := []string{}
	f, _ := os.Open(fileName)
	buf := bufio.NewReader(f)
	for {
		line, err := buf.ReadString('\n')
		line = strings.TrimSpace(line)
		re = append(re, line)
		if err != nil {
			if err == io.EOF {
				return re
			}
			return nil
		}
	}
}
